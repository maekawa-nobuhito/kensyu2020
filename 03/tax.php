<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
  </head>
  <div style="text-align: right">
  <body>
    <form method="POST" action="tax.php">
      <table border="1">
        <tbody>
          <thead>
            <tr>
              <th>商品名</th>
              <th>価格（単位：円、税抜き）</th>
              <th>個数</th>
              <th>税率</th>
            </tr>
          </thead>
            <tr>
              <td><input type="text" name="shinamono01"></td>
              <td><input type="text" name="kakaku01"></td>
              <td><input type="text" name="kazu01"></td>
              <td colspan="2">
                <input type="radio" name="tax01" value="1.08">0.8%
                <input type="radio" name="tax01" value="1.10">10%
              </td>
            </tr>
            <tr>
              <td><input type="text" name="shinamono02"></td>
              <td><input type="text" name="kakaku02"></td>
              <td><input type="text" name="kazu02"></td>
              <td colspan="2">
                <input type="radio" name="tax02" value="1.08">0.8%
                <input type="radio" name="tax02" value="1.10">10%
              </td>
            </tr>
            <tr>
              <td><input type="text" name="shinamono03"></td>
              <td><input type="text" name="kakaku03"></td>
              <td><input type="text" name="kazu03"></td>
              <td colspan="2">
                <input type="radio" name="tax03" value="1.08">0.8%
                <input type="radio" name="tax03" value="1.10">10%
              </td>
            </tr>
            <tr>
              <td><input type="text" name="shinamono04"></td>
              <td><input type="text" name="kakaku04"></td>
              <td><input type="text" name="kazu04"></td>
              <td colspan="2">
                <input type="radio" name="tax04" value="1.08">0.8%
                <input type="radio" name="tax04" value="1.10">10%
              </td>
            </tr>
            <tr>
              <td><input type="text" name="shinamono05"></td>
              <td><input type="text" name="kakaku05"></td>
              <td><input type="text" name="kazu05"></td>
              <td colspan="2">
                <input type="radio" name="tax05" value="1.08">0.8%
                <input type="radio" name="tax05" value="1.10">10%
              </td>
            </tr>
            <td colspan="4">
              <input type="submit"value="送信する">
              <input type="reset" value="リセット">
            </td>
        </tbody>
        <table border="1">
          <tbody>
            <tr>
              <th>商品名</th>
              <th>価格（単位：円、税抜き）</th>
              <th>個数</th>
              <th>税率</th>
              <th>小計</th>
            </tr>
            <tr>
            <td><?php echo $_POST['shinamono01'];?></td>
            <td><?php echo $_POST['kakaku01'];?></td>
            <td><?php echo $_POST['kazu01'];?></td>
            <td><?php echo $_POST['tax01'];?></td>
            <td><?php
                    $price1 = $_POST['kakaku01'];
                    $price2 = $_POST['kazu01'];
                    $tax = $_POST['tax01'];
                    $ans = $price1 * $price2;
                    $ans1 = $ans * $tax;
                    echo $ans1 . "円、税込み";?></td>
            </tr>
            <tr>
            <td><?php echo $_POST['shinamono02'];?></td>
            <td><?php echo $_POST['kakaku02'];?></td>
            <td><?php echo $_POST['kazu02'];?></td>
            <td><?php echo $_POST['tax02'];?></td>
            <td><?php
                    $price1 = $_POST['kakaku02'];
                    $price2 = $_POST['kazu02'];
                    $tax = $_POST['tax02'];
                    $ans = $price1 * $price2;
                    $ans2 = $ans * $tax;
                    echo $ans2 . "円、税込み";?></td>
            </tr>
            <tr>
            <td><?php echo $_POST['shinamono03'];?></td>
            <td><?php echo $_POST['kakaku03'];?></td>
            <td><?php echo $_POST['kazu03'];?></td>
            <td><?php echo $_POST['tax03'];?></td>
            <td><?php
                    $price1 = $_POST['kakaku03'];
                    $price2 = $_POST['kazu03'];
                    $tax = $_POST['tax03'];
                    $ans = $price1 * $price2;
                    $ans3 = $ans * $tax;
                    echo $ans3 . "円、税込み";?></td>
            </tr>
            <tr>
            <td><?php echo $_POST['shinamono04'];?></td>
            <td><?php echo $_POST['kakaku04'];?></td>
            <td><?php echo $_POST['kazu04'];?></td>
            <td><?php echo $_POST['tax04'];?></td>
            <td><?php
                    $price1 = $_POST['kakaku04'];
                    $price2 = $_POST['kazu04'];
                    $tax = $_POST['tax04'];
                    $ans = $price1 * $price2;
                    $ans4 = $ans * $tax;
                    echo $ans4 . "円、税込み";?></td>
            </tr>
            <tr>
            <td><?php echo $_POST['shinamono05'];?></td>
            <td><?php echo $_POST['kakaku05'];?></td>
            <td><?php echo $_POST['kazu05'];?></td>
            <td><?php echo $_POST['tax05'];?></td>
            <td><?php
                    $price1 = $_POST['kakaku05'];
                    $price2 = $_POST['kazu05'];
                    $tax = $_POST['tax05'];
                    $ans = $price1 * $price2;
                    $ans5 = $ans * $tax;
                    echo $ans5 . "円、税込み";?></td>
            </tr>
            <td>合計</td>
            <td colspan="4">
              <?php
              $a = $ans1;
              $b = $ans2;
              $c = $ans3;
              $d = $ans4;
              $e = $ans5;
              $f = $a + $b + $c + $d + $e;
              echo $f;
              ?>
            </td>
          </tbody>
        </table>
  </body>
 </div>
</html>

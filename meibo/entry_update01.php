<!DOCTYPE html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php

$DB_DSN = "mysql:host=localhost; dbname=kensyu_test; charset=utf8";
$DB_USER = "php_user";
$DB_PW = "HhtyebzjLNCRz16a";
$pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

$seach_ID = "";
$seach_name = "";
$seach_pref = "";
$seach_seibetu = "";
$seach_age = "";
$seach_section_name = "";
$seach_grade_name = "";
$seach_section_ID = "";
$seach_grade_ID = "";


if(isset($_GET['member_ID']) && $_GET['member_ID'] != ""){
    $seach_ID = $_GET['member_ID'];
 }
if(isset($_GET['name']) && $_GET['name'] != ""){
    $seach_name = $_GET['name'];
 }
if(isset($_GET['pref']) && $_GET['pref'] != ""){
    $seach_pref = $_GET['pref'];
 }
if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
    $seach_seibetu = $_GET['seibetu'];
 }
if(isset($_GET['age']) && $_GET['age'] != ""){
    $seach_age = $_GET['age'];
 }
if(isset($_GET['section_name']) && $_GET['section_name'] != ""){
    $seach_section_name = $_GET['section_name'];
 }
if(isset($_GET['grade_name']) && $_GET['grade_name'] != ""){
    $seach_grade_name = $_GET['grade_name'];
 }
 if(isset($_GET['section_ID']) && $_GET['section_ID'] != ""){
     $seach_section_ID = $_GET['section_ID'];
  }
 if(isset($_GET['grade_ID']) && $_GET['grade_ID'] != ""){
     $seach_grade_ID = $_GET['grade_ID'];
  }
  include("./include/statics.php");

  $query_str = "SELECT m.member_ID, m.name, m.seibetu, m.pref,m.age, m.section_ID,m.grade_ID
                FROM member AS m LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID
                WHERE m.member_ID =" . $seach_ID . "";

                //echo $query_str;
                $sql = $pdo->prepare($query_str);
                $sql->execute();
                $result = $sql->fetchAll();
                //var_dump($result);
  ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員情報編集</title>
    <?php include("./include/header.php"); ?>
  </head>
  <script type="text/javascript">
    <!--
    function conf(){
    var temp_name = document.update.name.value;
    temp_name =  temp_name.replace(/\s+/g,"");
    document.update.name.value = temp_name;
    if( temp_name == ""){
      alert("名前は必須です");
      return false;
    }

    //年齢上限下限チェック
    var temp_age = document.update.age.value;
    // alert(temp_age);
    if( temp_age == "" || Number.isInteger(temp_age)
        || temp_age < 1 || temp_age > 100){
      alert("年齢は必須です/数値を入力してください/1-99の範囲で入力してください");
      return false;
    }

    if(document.update.pref.value == ""){
      alert("都道府県は必須です");
      return false;
    }

    if(window.confirm('更新を行います。よろしいですか？')){
      document.update.submit();
      // alert("true");
    } else {
      // alert("false");
    //
    }
  }
    -->
    </script>

  <body>
   <form method="POST" action="update02.php" name="update">
     <hr/>
     <table border="0" class="table table-striped" style="width: 50%; margin: 0 auto;">
       <tr>
         <th>社員ID</th>
         <td><?php echo $result[0]['member_ID'];?></td>
       </tr>
       <tr>
         <th>名前:</th>
         <td><input type="text" name="name" value="<?php echo $result[0]['name'];?>"></td>
       </tr>
       <tr>
         <th>出身地:</th>
         <td><select name="pref">
           <?php
               foreach($pref_array as $key => $value){
                 echo "<option value='" . $key . "'";
                 if($result[0]['pref'] == $key){ echo " selected ";}
                 echo ">" . $value . "</option>";
           }
           ?>
         </select></td>
       </tr>
       <tr>
         <th>性別:</th>
         <td><input type="radio" name="seibetu" <?php if ($result[0]['seibetu'] == "1") { echo "checked"; } ?> value="1" id="sex01"><label for='sex01'>男</label>
             <input type="radio" name="seibetu" <?php if ($result[0]['seibetu'] == "2") { echo "checked"; } ?> value="2" id="sex02"><label for='sex02'>女</label>
         </td>
       </tr>
       <tr>
         <th>年齢:</th>
         <td><input type="number" name="age" value="<?php echo $result[0]['age'];?>" placeholder="年齢"></td></td>
       </tr>
       <tr>
         <th>所属部署:</th>
         <td><input type="radio" name="section_ID" <?php if ($result[0]['section_ID'] == "1") { echo "checked"; } ?> value="1" id="sec01"><label for='sec01'>第一事業部</label>
             <input type="radio" name="section_ID" <?php if ($result[0]['section_ID'] == "2") { echo "checked"; } ?> value="2" id="sec02"><label for='sec02'>第二事業部</label>
             <input type="radio" name="section_ID" <?php if ($result[0]['section_ID'] == "3") { echo "checked"; } ?> value="3" id="sec03"><label for='sec03'>営業</label>
             <input type="radio" name="section_ID" <?php if ($result[0]['section_ID'] == "4") { echo "checked"; } ?> value="4" id="sec04"><label for='sec04'>総務</label>
             <input type="radio" name="section_ID" <?php if ($result[0]['section_ID'] == "5") { echo "checked"; } ?> value="5" id="sec05"><label for='sec05'>人事</label>
         </td>
       </tr>
       <tr>
         <th>役職:</th>
         <td><input type="radio" name="grade_ID" <?php if ($result[0]['grade_ID'] == "1") { echo "checked"; } ?> value="1" id="gra01"><label for='gra01'>事業部長</label>
             <input type="radio" name="grade_ID" <?php if ($result[0]['grade_ID'] == "2") { echo "checked"; } ?> value="2" id="gra02"><label for='gra02'>部長</label>
             <input type="radio" name="grade_ID" <?php if ($result[0]['grade_ID'] == "3") { echo "checked"; } ?> value="3" id="gra03"><label for='gra03'>チームリーダー</label>
             <input type="radio" name="grade_ID" <?php if ($result[0]['grade_ID'] == "4") { echo "checked"; } ?> value="4" id="gra04"><label for='gra04'>リーダー</label>
             <input type="radio" name="grade_ID" <?php if ($result[0]['grade_ID'] == "5") { echo "checked"; } ?> value="5" id="gra05"><label for='gra05'>メンバー</label>
         </td>
       </tr>
     </table>
     <div style="width: 34%;float: right;margin: 0px auto;">
         <input type="button" value="更新" onclick="conf();" class="btn btn-primary">
         <input type="reset" value="クリア" class="btn btn-outline-primary">
         <input type="hidden" value="<?php echo $result[0]['member_ID'];?>" name="member_ID">
     </div>
   </form>
  </body>
</html>

<!DOCTYPE html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php include("./include/statics.php");?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新規社員登録</title>
    <?php include("./include/header.php"); ?>
  </head>
    <script type="text/javascript">
      <!--
      function conf(){

        //名前からスペースの除去、それから入力チェック
        var temp_name = document.entry.name.value;
        temp_name =  temp_name.replace(/\s+/g,"");
        document.entry.name.value = temp_name;
        if( temp_name == ""){
          alert("名前は必須です");
          return false;
        }

        //年齢上限下限チェック
        var temp_age = document.entry.age.value;
        // alert(temp_age);
        if( temp_age == "" || Number.isInteger(temp_age)
            || temp_age < 1 || temp_age > 100){
          alert("年齢は必須です/数値を入力してください/1-99の範囲で入力してください");
          return false;
        }

        if(document.entry.pref.value == ""){
          alert("都道府県は必須です");
          return false;
        }

        if(window.confirm('新規社員の登録を行います。よろしいですか？')){
          document.entry.submit();
          // alert("true");
        } else {
          // alert("false");
        //
        }
      }


      -->
      </script>

  <body>
    <hr/>
   <form method="GET" action="entry02.php" name="entry" >
     <table border="0" class="table table-striped" style="width: 50%; margin: 0 auto;">
       <tr>
         <th>名前:</th>
         <td><input type="text" name="name" placeholder="新規登録者氏名"></td>
       </tr>
       <tr>
         <th>出身地:</th>
         <td><select name="pref">
              <option value="">都道府県</option>
              <option value="1">北海道</option>
              <option value="2">青森県</option>
              <option value="3">岩手県</option>
              <option value="4">宮城県</option>
              <option value="5">秋田県</option>
              <option value="6">山形県</option>
              <option value="7">福島県</option>
              <option value="8">茨城県</option>
              <option value="9">栃木県</option>
              <option value="10">群馬県</option>
              <option value="11">埼玉県</option>
              <option value="12">千葉県</option>
              <option value="13">東京都</option>
              <option value="14">神奈川県</option>
              <option value="15">新潟県</option>
              <option value="16">富山県</option>
              <option value="17">石川県</option>
              <option value="18">福井県</option>
              <option value="19">山梨県</option>
              <option value="20">長野県</option>
              <option value="21">岐阜県</option>
              <option value="22">静岡県</option>
              <option value="23">愛知県</option>
              <option value="24">三重県</option>
              <option value="25">滋賀県</option>
              <option value="26">京都府</option>
              <option value="27">大阪府</option>
              <option value="28">兵庫県</option>
              <option value="29">奈良県</option>
              <option value="30">和歌山県</option>
              <option value="31">鳥取県</option>
              <option value="32">島根県</option>
              <option value="33">岡山県</option>
              <option value="34">広島県</option>
              <option value="35">山口県</option>
              <option value="36">徳島県</option>
              <option value="37">香川県</option>
              <option value="38">愛媛県</option>
              <option value="39">高知県</option>
              <option value="40">福岡県</option>
              <option value="41">佐賀県</option>
              <option value="42">長崎県</option>
              <option value="43">熊本県</option>
              <option value="44">大分県</option>
              <option value="45">宮崎県</option>
              <option value="46">鹿児島県</option>
              <option value="47">沖縄県</option>
         </select></td>
       </tr>
       <tr>
         <th>性別:</th>
         <td><input type="radio" name="seibetu" value="1" checked id="sex01"><label for='sex01'>男</label>
             <input type="radio" name="seibetu" value="2" id="sex02"><label for='sex02'>女</label>
         </td>
       </tr>
       <tr>
         <th>年齢:</th>
         <td><input type="number" name="age" placeholder="年齢"></td></td>
       </tr>
       <tr>
         <th>所属部署:</th>
         <td><input type="radio" name="section_ID" value="1" checked id="sec01"><label for='sec01'>第一事業部</label>
             <input type="radio" name="section_ID" value="2" id="sec02"><label for='sec02'>第二事業部</label>
             <input type="radio" name="section_ID" value="3" id="sec03"><label for='sec03'>営業</label>
             <input type="radio" name="section_ID" value="4" id="sec04"><label for='sec04'>総務</label>
             <input type="radio" name="section_ID" value="5" id="sec04"><label for='sec05'>人事</label>
         </td>
       </tr>
       <tr>
         <th>役職:</th>
         <td><input type="radio" name="grade_ID" value="1" checked id="gra01"><label for='gra01'>事業部長</label>
             <input type="radio" name="grade_ID" value="2" id="gra02"><label for='gra02'>部長</label>
             <input type="radio" name="grade_ID" value="3" id="gra03"><label for='gra03'>チームリーダー</label>
             <input type="radio" name="grade_ID" value="4" id="gra04"><label for='gra04'>リーダー</label>
             <input type="radio" name="grade_ID" value="5" id="gra05"><label for='gra05'>メンバー</label>
         </td>
       </tr>
     </table>
       <div style="width: 34%;float: right;margin: 0px auto;">
         <input type="button" value="登録" class="btn btn-primary" onclick="conf();">
         <input type="reset" value="クリア" class="btn btn-outline-primary">
       </div>
   </form>
  </body>
</html>

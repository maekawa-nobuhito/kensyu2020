<!DOCTYPE html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_test; charset=utf8";
  $DB_USER = "php_user";
  $DB_PW = "HhtyebzjLNCRz16a";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $seach_ID = "";

  if(isset($_GET['member_ID']) && $_GET['member_ID'] != ""){
      $seach_ID = $_GET['member_ID'];
   }

  $query_str = "SELECT m.member_ID, m.name, m.seibetu, m.pref,m.age,gm.grade_name, sm.section_name
                FROM member AS m LEFT JOIN section1_master AS sm ON m.section_ID = sm.ID LEFT JOIN grade_master AS gm ON m.grade_ID = gm.ID
                WHERE m.member_ID =" . $seach_ID . "";


  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  //var_dump($result);


  include("./include/statics.php");
  ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員情報詳細画面</title>
    <?php include("./include/header.php"); ?>
  </head>
  <script type="text/javascript">
    <!--
    function goDel(id){
      if(window.confirm('登録情報の削除を行います。よろしいですか？')){
        location.href = "./delete01.php?member_ID=" + id;
      }
    }
    function goEdit(id){
      location.href = "./entry_update01.php?member_ID=" + id;
    }


-->
</script>

  <body>
  <hr/>
   <form method="GET" action="index.php" >
     <table class="table table-striped" border="0" style="width: 50%; margin: 0 auto;">
       <tr>
         <th scope="row">社員ID:</th>
         <td><?php echo $result[0]['member_ID'];?></td>
       </tr>
       <tr>
        <th scope="row">名前:</th>
        <td><?php echo $result[0]['name'];?></td>
       </tr>
       <tr>
        <th scope="row">出身地:</th>
        <td><?php echo $pref_array[$result[0]['pref']];?></td>
       </tr>
       <tr>
        <th scope="row">性別:</th>
        <td><?php echo $gender_array[$result[0]['seibetu']];?></td>
       </tr>
       <tr>
        <th scope="row">年齢:</th>
        <td><?php echo $result[0]['age'];?></td>
       </tr>
       <tr>
        <th scope="row">所属部署:</th>
        <td><?php echo $result[0]['section_name'];?></td>
       </tr>
       <tr>
        <th scope="row">役職:</th>
        <td><?php echo $result[0]['grade_name'];?></td>
       </tr>
     </table>
     <div div style="width: 34%;float: right;margin: 0px auto;">
         <input type="button" class="btn btn-primary" value="編集" onclick="goEdit(<?php echo $result[0]['member_ID']; ?>);">
         <input type="button" class="btn btn-outline-danger" value="削除" onclick="goDel(<?php echo $result[0]['member_ID']; ?>);">
         <input type="hidden" value="<?php echo $result[0]['member_ID'];?>" name="member_ID">
     </div>
   </form>
 </body>
</html>
